A theme for E25.4 and up that mixes or adheres reasonably well with Elive E16 Retrowave edition.

To install/use either copy the Elive-E25-Retro.edje to your $HOME/.elementary/themes/ folder or ...
Download the edje and import it through the "settings", "theme" dialog.

If you want to play with or change things then "clone" this repo, make your changes in the Elive-E25-Retro/ folder and use the "build.sh" command in there, to create a new .edje to distribute.
Hint: Rename the resulting .edje file though, to avoid mishaps later.

The "Elive-Vitamnin-c" theme is an orange version of the Retro theme.
