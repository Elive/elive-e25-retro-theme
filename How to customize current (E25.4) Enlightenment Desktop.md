## How to customize current (E25.4) Enlightenment Desktop

- **and why you should run it on Elive.**



As many know, Elive uses E16 as the default desktop whilst awaiting a stable iteration of the, curently at E25.4 Enlightenment Desktop. 

This is good thinking as E16 is Stable, Fast, lightweight and easy to use but there's a downside. *It keeps potential users away from E25 and consequently means less bug hunting or reporting. Slowing down the development even more.*

While some major distros like Debian, Red Hat and SuSe have the option to install and use the environment next to their default desktop there's only two distributions that offer Enlightenment by default:

Bhodi that uses a forked E17 (reasonably in line with newer E versions) desktop and Elive that comes with E16. 

- **The world is in need of a dedicated Enlightenment distro**

While there are some distros, like [Extontlinux](https://exlight.exton.net/) and mxlinux out there that showcase (amongst others) an Enlightenment pre-installed desktop. None offer much more than the default dark-themed E25, excepting OpenSuse that has done some nice branding on the theme but still requires you to download and install Enlightenment  yourself.... as sadly still does Elive too, by the way.

The downside of this DIY method is that the underlying system has some flaws and multiple applications doing the same or even getting in one another's way: Think "connman" and "network-manager" (where one or the other has to be disabled), multiple file managers or (on Elive) multiple settings applications that don't affect Enlightenment.

A dedicated Elive-Beta E25 Enlightenment version, leaving out E16 specifics (maybe pulling in some older compatible E17 stuff?) and having a   a nicely themed and Elive branded E25 could be beneficial to Elive as well as Enlightenment itself.

- **Theming and branding E to co-exist with E16 Retrowave**

Awaiting a dedicated Elive-E25+ release we can install the latest Enlightenment from a terminal with a simple: `api elightenment` command.

and a first time run will open to:

[![image|1920x1280,30%](https://i.imgur.com/Q6qQqaR.jpg)](https://i.imgur.com/Q6qQqaR.jpg)

But I want to create something along the lines of this:

[![image|1920x1280,30%](https://i.imgur.com/g4SdAyC.jpg) ](https://i.imgur.com/g4SdAyC.jpg)

As a first I altered the E25 theme, making it darker and going a more magenta/cyan color theme to correspond to the E16 Retrowave theme. I made extensive use of Simon Lees aka [Simotek's scripts](https://github.com/simotek/Enlightenment-Themes) to alter the theme to my wishes.

His green ["NeonZ"](https://github.com/simotek/Enlightenment-Themes/releases/tag/20220219.1.26) is quite excellent as is his "[OpenSuse-e](https://github.com/simotek/Enlightenment-Themes/releases/tag/20220219.1.26)" theme.

Alas a green setting of Elive would bring us too close to Bodhi or OpenSuse branding which led to the "Elive-E25-Retro" theme as shown above. Downloading and importing the Elive theme(get it [here](https://gitlab.com/Elive/elive-e25-retro-theme/-/raw/main/Elive-E25-Retro.edj)) will not make a real difference and show up something like this:

[![image|1920x1280,30%](https://i.imgur.com/hthfNQu.jpg)](https://i.imgur.com/hthfNQu.jpg)



Now there's a lot to be changed there and most of it can be done from within E itself.

1. I personally don't like those icons on my desktop. I find them clunky and ugly, so let's remove them by unchecking the appropriate checkbox.
   - [![image|1920x1280,30%](https://i.imgur.com/vDtGkMJ.png)](https://i.imgur.com/vDtGkMJ.png) 

2. The default E icon set isn't a good fit with Retrowave so we'll see about using the "beauty-line" icon set that E16 has available.

   - ![image|1920x1280,30%](https://i.imgur.com/dpQxJcE.png)](https://i.imgur.com/dpQxJcE.png) 

   Making the icons and color settings align a bit more.

3. Next we'll give the iBar down below a make-over by splitting it into 3 different shelves. Each with different content like  the pager that gives access to different desktops as well as a separate systray.

   - The pager in the bar is fairly small and doesn't really give access to nice stuff like dragging apps from on desktop to another. So let's get it out of there and set it top-right, similar to the old E17 pager that Stable had.

     That would mean creating a new shelf from the "Settings", "Extensions", "shelves" dialog and creating a new one (or two while we're at it).

     - [![image|1920x1280,30%](https://i.imgur.com/LOmP74g.png)](https://i.imgur.com/LOmP74g.png)

     One shelf for the pager and a second shelf to contain the systray. Set style, size and orientate both shelves to your liking by playing around with the settings.

     Oh and don't forget to remove the pager and the systray from the original shelf. :)

   - The "Start" button (or arrow) on the left of the iBar does the same as a single-click anywhere on the dekstop does, which is kind of an unnecessary thing. Might as well "remove" it through a right click and add our own, with an Elive logo.

     We'll need a .desktop file to add to the iBar and we'll have to make ourselves, which isn't that hard.

     All that's needed is a file called "starter.desktop" with the following content:

     

     `[Desktop Entry]`
     `Type=Application`
     `Encoding=UTF-8`

     `Name=App Launcher`

     `GenericName=Starter`
     `Comment=Lightweight starter`

     `Exec=ulauncher`
     `Icon=elive-logo`
     `Terminal=false`
     `Categories=System;`

     Save it to somewhere, where you can find it or (as root) into "/usr/share/applications/" for system wide access.

     And simply drag&drop into your iBar for future use.

     - [![image|1920x1280,30%](https://i.imgur.com/MSdHCtS.png)](https://i.imgur.com/MSdHCtS.png)

     Personally I use a modified "rofi" as App Launcher, noticeable in the screenshot but that requires installing the app itself. It doesn't come pre-installed on Elive like "ulauncher".

   - The wallpaper inside the theme isn't rendered as nice as it should be (that's a TODO) so it's advisable the get the original [e_wallpaper.png](https://gitlab.com/Elive/elive-e25-retro-theme/-/raw/main/images/e_wallpaper.png?inline=false) and import it as is.

     The original is [here](https://wallpapercave.com/download/open-source-wallpapers-wp7596336) but alas there's a small typo in there. The i between L and the T in "POSSIBILITIES" got left out.

     So I sort of repaired/added it with a slighter smaller L as a consequence, which was a lot easier than moving the rest to the left or right. 

4.  The shelf containing the pager will contain 4 desktops in a row, as they were when they were part of the iBar shelf.

   To have more rows you need to go to "settings", "screens","virtual desktops" and use the sliders there until you're happy.

   - [![image|1920x1280,30%](https://i.imgur.com/gP3eOrY.png)](https://i.imgur.com/gP3eOrY.png)

   Then resize and style that shelf/pager to your liking. Where I'd advise to make it big enough to discern the contained open apps and set it it "autohide" so that it doesn't take away too much screen real estate. Opened new windows will "dodge" the shelf if it's visible and set at a top level. Setting it at a  lower (or lowest) level would make it inaccessible if a window blocks it from view.

5. The clock in the systray shelf is set to use the digital version but there's also an analog version. Where if you're feeling nostalgicly inclined to old-Stable's clock on the desktop .... you can create an extra shelf containing just that and have it hover somewhere on the desktop. The downside is that there'll always be something covering it, requiring a shift to a clean desktop to read the time.

   - ![](https://i.imgur.com/reBUsrs.png)](https://i.imgur.com/reBUsrs.png)

6. If you've got arunning E16 alongside (which you probably have on Elive) you'll have "network-manager" in use, not the "connman" that  E expects.

   To get the icon and the network options to show in your systray is a matter of `"nm-applet --indicator &"` in a terminal. If it doesn't show, then it might already be running and will first need to be stopped with `"killal nm-applet". After that you can run the first command again to make it show.

   - [![image|1920x1280,30%](https://i.imgur.com/dckUt7L.png)](https://i.imgur.com/dckUt7L.png)

   

   This finicky little issue makes it unusable to "autostart" in any way when logging in to your desktop, which is a pity.

    





